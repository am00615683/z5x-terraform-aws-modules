####################################################################
#### Created by : Balakrishna ####
#### Date : 13-Seo-2021 ####
####################################################################
####################    VPC    ####################
data "aws_vpc" "vpc" {
  tags = {
    Name = var.vpc_id
  }
}

#################### PUB Layer ####################
data "aws_subnet" "pub_subnet_1" {
  vpc_id = data.aws_vpc.vpc.id
  filter {
    name   = "tag:Name"
    values = ["${var.search_pattern}-public-a"]
  }
}

data "aws_subnet" "pub_subnet_2" {
  vpc_id = data.aws_vpc.vpc.id
  filter {
    name   = "tag:Name"
    values = ["${var.search_pattern}-public-b"]
  }
}

data "aws_subnet_ids" "pub" {
  vpc_id = data.aws_vpc.vpc.id
  filter {
    name   = "tag:Name"
    values = ["${var.search_pattern}-public-*"]
  }
}
#################### Private Layer ####################
data "aws_subnet" "pri_subnet_1" {
  vpc_id = data.aws_vpc.vpc.id
  filter {
    name   = "tag:Name"
    values = ["${var.search_pattern}-internal-a"]
  }
}

data "aws_subnet" "pri_subnet_2" {
  vpc_id = data.aws_vpc.vpc.id
  filter {
    name   = "tag:Name"
    values = ["${var.search_pattern}-internal-a"]
  }
}

data "aws_subnet_ids" "pri" {
  vpc_id = data.aws_vpc.vpc.id
  filter {
    name   = "tag:Name"
    values = ["${var.search_pattern}-internal-*"]
  }
}