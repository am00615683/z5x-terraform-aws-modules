output "vpc_id" {
  value = data.aws_vpc.vpc.id
}
output "vpc_id_cidr" {
  value = data.aws_vpc.vpc.cidr_block
}
output "subnet_sharedsp_az1_id" {
  value = data.aws_subnet.sharedsp_subnet_1.id
}
output "subnet_sharedsp_az2_id" {
  value = data.aws_subnet.sharedsp_subnet_2.id
}