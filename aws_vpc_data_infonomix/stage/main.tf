####################################################################
#### Created by : Kalyan Chakravarthi/ Kalyan Bhave ####
#### Date : 20-Jun-2020 ####
#### Updated by : Balakrishna/ Praveen kumar/ Sathish kumar ####
####################################################################
####################    VPC    ####################
data "aws_vpc" "vpc" {
  tags = {
    Name = var.vpc_id
  }
}

#################### PUB Layer ####################
data "aws_subnet" "pub_subnet_1" {
  vpc_id = data.aws_vpc.vpc.id
  filter {
    name   = "tag:Name"
    values = ["${var.search_pattern}-pub-az1"]
  }
}
data "aws_subnet" "pub_subnet_2" {
  vpc_id = data.aws_vpc.vpc.id
  filter {
    name   = "tag:Name"
    values = ["${var.search_pattern}-pub-az2"]
  }
}
###################shared##########
data "aws_subnet" "shared_subnet_1" {
  vpc_id = data.aws_vpc.vpc.id
  filter {
    name   = "tag:Name"
    values = ["${var.search_pattern}-pri-shared-az1"]
  }
}
data "aws_subnet" "shared_subnet_2" {
  vpc_id = data.aws_vpc.vpc.id
  filter {
    name   = "tag:Name"
    values = ["${var.search_pattern}-pri-shared-az2"]
  }
}

###################### compute #################################################
data "aws_subnet" "compute_subnet_1" {
  vpc_id = data.aws_vpc.vpc.id
  filter {
    name   = "tag:Name"
    values = ["${var.search_pattern}-pri-compute-az1"]
  }
}
data "aws_subnet" "compute_subnet_2" {
  vpc_id = data.aws_vpc.vpc.id
  filter {
    name   = "tag:Name"
    values = ["${var.search_pattern}-pri-compute-az2"]
  }
}
###################### db #################################################
data "aws_subnet" "db_subnet_1" {
  vpc_id = data.aws_vpc.vpc.id
  filter {
    name   = "tag:Name"
    values = ["${var.search_pattern}-pri-db-az1"]
  }
}
###################### redis #################################################
data "aws_subnet" "redis_subnet_1" {
  vpc_id = data.aws_vpc.vpc.id
  filter {
    name   = "tag:Name"
    values = ["${var.search_pattern}-pri-redis-az1"]
  }
}
data "aws_subnet" "redis_subnet_2" {
  vpc_id = data.aws_vpc.vpc.id
  filter {
    name   = "tag:Name"
    values = ["${var.search_pattern}-pri-redis-az2"]
  }
}