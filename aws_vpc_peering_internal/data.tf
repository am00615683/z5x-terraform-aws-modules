data "aws_caller_identity" "peer" {
  provider = aws.peer
}
data "aws_vpc" "main" {
  provider = aws.peer
  filter {
    name   = "tag:Name"
    values = ["zee5-PublisherDashboard-vpc"]
  }
}

############################ pd-prod-rtb-pri-redis############################################
data "aws_route_table" "redis-prd-rtb-pri-az1" {
  provider = aws.peer
  vpc_id   = data.aws_vpc.vpcid.id
  filter {
    name   = "tag:Name"
    values = ["pd-prod-rtb-pri-redis-az1"]
  }
}
data "aws_route_table" "redis-prd-rtb-pri-az2" {
  provider = aws.peer
  vpc_id = data.aws_vpc.vpcid.id
  filter {
    name = "tag:Name"
    values = ["pd-prod-rtb-pri-redis-az2"]
  }
}
data "aws_route_table" "redis-prd-rtb-pri-az3" {
  provider = aws.peer
  vpc_id   = data.aws_vpc.vpcid.id
  filter {
    name   = "tag:Name"
    values = ["pd-prod-rtb-pri-redis-az3"]
  }
}
############################ pd-prod-rtb-pri-rds############################################
data "aws_route_table" "rds-prd-rtb-pri-az1" {
  provider = aws.peer
  vpc_id   = data.aws_vpc.vpcid.id
  filter {
    name   = "tag:Name"
    values = ["pd-prod-rtb-pri-rds-az1"]
  }
}
data "aws_route_table" "rds-prd-rtb-pri-az2" {
  provider = aws.peer
  vpc_id = data.aws_vpc.vpcid.id
  filter {
    name = "tag:Name"
    values = ["pd-prod-rtb-pri-rds-az2"]
  }
}
data "aws_route_table" "rds-prd-rtb-pri-az3" {
  provider = aws.peer
  vpc_id   = data.aws_vpc.vpcid.id
  filter {
    name   = "tag:Name"
    values = ["pd-prod-rtb-pri-rds-az3"]
  }
}
############################ pd-prod-rtb-pri-compute############################################
data "aws_route_table" "compute-prd-rtb-pri-az1" {
  provider = aws.peer
  vpc_id   = data.aws_vpc.vpcid.id
  filter {
    name   = "tag:Name"
    values = ["pd-prod-rtb-pri-compute-az1"]
  }
}
data "aws_route_table" "compute-prd-rtb-pri-az2" {
  provider = aws.peer
  vpc_id = data.aws_vpc.vpcid.id
  filter {
    name = "tag:Name"
    values = ["pd-prod-rtb-pri-compute-az2"]
  }
}
data "aws_route_table" "compute-prd-rtb-pri-az3" {
  provider = aws.peer
  vpc_id   = data.aws_vpc.vpcid.id
  filter {
    name   = "tag:Name"
    values = ["pd-prod-rtb-pri-compute-az3"]
  }
}

data "aws_vpc" "vpcid" {
  provider = aws.peer
  filter {
    name   = "tag:Name"
    values = ["pd-prod-vpc"]
  }
}
