##############  Date : 19-Jan-2020 ##############
#### Created by : Balakrishna/ Praveen kumar ####
#################################################
####################    VPC    ##################
data "aws_vpc" "vpc" {
  tags = {
    Name = var.vpc_id
  }
}

#################### PUB Layer ####################
data "aws_subnet" "pub_subnet_1" {
  vpc_id = data.aws_vpc.vpc.id
  filter {
    name   = "tag:Name"
    values = ["${var.search_pattern}-pub1-ap1a"]
  }
}

data "aws_subnet" "pub_subnet_2" {
  vpc_id = data.aws_vpc.vpc.id
  filter {
    name   = "tag:Name"
    values = ["${var.search_pattern}-pub2-ap1b"]
  }
}
data "aws_subnet_ids" "pub" {
  vpc_id = data.aws_vpc.vpc.id
  filter {
    name   = "tag:Name"
    values = ["${var.search_pattern}-pub*"]
  }
}

#################### DB Layer ####################
data "aws_subnet" "db_subnet_1" {
  vpc_id = data.aws_vpc.vpc.id
  filter {
    name   = "tag:Name"
    values = ["${var.search_pattern}-db1-ap1a"]
  }
}

data "aws_subnet" "db_subnet_2" {
  vpc_id = data.aws_vpc.vpc.id
  filter {
    name   = "tag:Name"
    values = ["${var.search_pattern}-db2-ap1b"]
  }
}
data "aws_subnet_ids" "db" {
  vpc_id = data.aws_vpc.vpc.id
  filter {
    name   = "tag:Name"
    values = ["${var.search_pattern}-db*"]
  }
}
#################### Private Layer ####################
data "aws_subnet" "pri_subnet_1" {
  vpc_id = data.aws_vpc.vpc.id
  filter {
    name   = "tag:Name"
    values = ["${var.search_pattern}-pri1-ap1a"]
  }
}

data "aws_subnet" "pri_subnet_2" {
  vpc_id = data.aws_vpc.vpc.id
  filter {
    name   = "tag:Name"
    values = ["${var.search_pattern}-pri2-ap1b"]
  }
}

data "aws_subnet" "pri_subnet_3" {
  vpc_id = data.aws_vpc.vpc.id
  filter {
    name   = "tag:Name"
    values = ["${var.search_pattern}-pri3-ap1a"]
  }
}

data "aws_subnet" "pri_subnet_4" {
  vpc_id = data.aws_vpc.vpc.id
  filter {
    name   = "tag:Name"
    values = ["${var.search_pattern}-pri4-ap1b"]
  }
}

data "aws_subnet" "pri_subnet_7" {
  vpc_id = data.aws_vpc.vpc.id
  filter {
    name   = "tag:Name"
    values = ["${var.search_pattern}-pri7-ap1a"]
  }
}

data "aws_subnet" "pri_subnet_8" {
  vpc_id = data.aws_vpc.vpc.id
  filter {
    name   = "tag:Name"
    values = ["${var.search_pattern}-pri8-ap1b"]
  }
}
data "aws_subnet_ids" "pri" {
  vpc_id = data.aws_vpc.vpc.id
  filter {
    name   = "tag:Name"
    values = ["${var.search_pattern}-pri*"]
  }
}