####################################################################
#### Created by : Kalyan Bhave ####
#### Date : 20-Jun-2020 ####
#### Updated by : Balakrishna/ Praveen kumar/ Sathish kumar ####
####################################################################
########### Auctions ###############
resource "aws_launch_template" "lc_spot" {
  name_prefix   = var.name
  image_id      = var.image_id
  instance_type = var.instance_type
  user_data     = base64encode(data.template_file.user_data.rendered)
  key_name      = var.key_name
  dynamic "block_device_mappings" {
    for_each = var.block_device_mappings
    content {
      device_name  = lookup(block_device_mappings.value, "device_name", null)

      dynamic "ebs" {
        for_each = lookup(block_device_mappings.value, "ebs", null) == null ? [] : ["ebs"]
        content {
          delete_on_termination = lookup(block_device_mappings.value.ebs, "delete_on_termination", null)
          encrypted             = lookup(block_device_mappings.value.ebs, "encrypted", null)
          iops                  = lookup(block_device_mappings.value.ebs, "iops", null)
          kms_key_id            = lookup(block_device_mappings.value.ebs, "kms_key_id", null)
          snapshot_id           = lookup(block_device_mappings.value.ebs, "snapshot_id", null)
          volume_size           = lookup(block_device_mappings.value.ebs, "volume_size", null)
          volume_type           = lookup(block_device_mappings.value.ebs, "volume_type", null)
        }
      }
    }
  }
  iam_instance_profile {
    arn = "arn:aws:iam::${var.account_id}:instance-profile/${var.instance_profile_role_name}"
  }
  monitoring {
    enabled = true
  }
  vpc_security_group_ids = var.security_group
  tag_specifications {
    resource_type = var.resource_type

    tags = {
      Created_using = var.Created_using
      Environment         = var.environment
      app-name            = var.app-name
      team-name           = var.team-name
      created-by          = var.created-by
      ticket              = var.ticket
      cost-center         = var.cost-center
      contact-email       = var.contact-email
      created-date        = var.created-date
      resource-function   = var.resource-function
      patch-exempt        = var.patch-exempt
      data-classification = var.data-classification
      patch-group         = var.patch-group
    }
  }
}
data "template_file" "user_data" {
  template = file("templates/${var.user_data}.tpl")
}
