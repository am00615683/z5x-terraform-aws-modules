resource "aws_default_route_table" "public" {
  default_route_table_id = aws_vpc.core_vpc.default_route_table_id
  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.core_igw.id
  }

  tags = merge(
  map("Name", "${var.environment}-rtb-${var.sub_services_names["public"]}-all"),
  map("VPC", aws_vpc.core_vpc.id),
  var.common_tags,
  map("Classification", "public")
  )
}
resource "aws_route_table" "private_sharedsp" {
  count  = length(var.availability_zones)
  vpc_id = aws_vpc.core_vpc.id

  route {
    cidr_block     = "0.0.0.0/0"
    nat_gateway_id = element(aws_nat_gateway.core_nat.*.id, count.index)
  }
  route {
    cidr_block                = var.vpc_peering_cidr
    vpc_peering_connection_id = data.aws_vpc_peering_connection.pc.id
  }
#  route {
#    cidr_block                = var.vpc_peering_region_cidr
#    vpc_peering_connection_id = data.aws_vpc_peering_connection.region.id
#  }
  tags = merge(
  map("Name", "${var.environment}-rtb-${var.sub_services_names["private_sharedsp"]}-az${count.index + 1}"),
  map("VPC", aws_vpc.core_vpc.tags.Name),
  var.common_tags,
  map("Classification", "private")
  )
}

resource "aws_route_table_association" "private" {
  count          = length(var.availability_zones)
  subnet_id      = element(aws_subnet.subnet_private_sharedsp.*.id, count.index)
  route_table_id = element(aws_route_table.private_sharedsp.*.id, count.index)
}
################## VPC peering ###########################
data "aws_vpc_peering_connection" "pc" {
  filter {
    name   = "tag:Name"
    values = ["infonomix-prod-peering"]
  }
}
#data "aws_vpc_peering_connection" "region" {
#  filter {
#    name   = "tag:Name"
#    values = ["Mwaa-prod-peering"]
#  }
#}