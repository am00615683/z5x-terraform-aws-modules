data "aws_caller_identity" "peer" {
  provider = aws.peer
}
data "aws_vpc" "main" {
  filter {
    name   = "tag:Name"
    values = ["MWAA-stage-vpc1"]
  }
}
data "aws_vpc" "vpcid" {
  provider = aws.peer
  filter {
    name   = "tag:Name"
    values = ["CDP-development/cdp-development-vpc"]
  }
}
